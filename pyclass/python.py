# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from typing import List, Optional
from .exec import Exec


def make_exec(path):
    mode = os.stat(path).st_mode
    mode = mode | (mode & 0o444) >> 2
    os.chmod(path, mode)


class Python(Exec):
    """
    Pybuild class for python scripts that allows dependencies to be installed using
    a virtualenv

    Note that these scripts must be executed as executables.
    No mechanism for importing them is provided
    """

    # Should be a python file that can be executed directly in windows, or indirectly
    # via the python interpreter on other platforms
    PYTHON_EXEC: str
    WRAPPER_NAME: Optional[str]
    WRAPPER_DIR: Optional[str]
    # A list of python dependencies to be installed using pip within the virtualenv
    # FIXME: Caching dependency downloads would be useful
    PYTHON_RDEPEND: List[str] = []

    def __init__(self):
        self.IUSE.add("platform_win32")
        self.IUSE_EFFECTIVE.add("platform_win32")
        self.DEPEND = self.DEPEND + " sys-bin/virtualenv"

    def src_unpack(self):
        super().src_unpack()
        if "platform_win32" in self.USE:
            with open(os.path.join(self.T, "install_deps.bat"), "w") as file:
                print("virtualenv .venv", file=file)
                print("call .venv/Scripts/activate.bat", file=file)
                for dep in self.PYTHON_RDEPEND:
                    print(f"pip install {dep}", file=file)
            self.execute(os.path.join(self.T, "install_deps.bat"))
        else:
            with open(os.path.join(self.T, "install_deps.sh"), "w") as file:
                print("#!/bin/sh", file=file)
                print("virtualenv .venv", file=file)
                print("source .venv/bin/activate", file=file)
                for dep in self.PYTHON_RDEPEND:
                    # Use python rather than pip directly
                    # since working directory may be NOEXEC
                    print(f"python .venv/bin/pip install {dep}", file=file)
            self.execute("sh " + os.path.join(self.T, "install_deps.sh"))

    def src_prepare(self):
        super().src_prepare()
        wrapper_dir = self.WRAPPER_DIR or "bin"
        os.makedirs(wrapper_dir, exist_ok=True)
        os.rename(
            os.path.join(self.WORKDIR, ".venv"), os.path.join(wrapper_dir, ".venv"),
        )

        # Copy wrapper command to bin directory
        if "platform_win32" in self.USE:
            path = os.path.join(wrapper_dir, self.WRAPPER_NAME or self.MN + ".bat")
            with open(path, "w") as file:
                print("@echo off", file=file)
                print("call %~f0/../.venv/Scripts/activate.bat", file=file)
                relpath = os.path.relpath(self.PYTHON_EXEC, os.path.dirname(path))
                print(f"python %~f0\\..\\{relpath} %*", file=file)
        else:
            path = os.path.join(wrapper_dir, self.WRAPPER_NAME or self.MN)
            with open(path, "w") as file:
                print("#!/bin/sh", file=file)
                print('CURDIR=`dirname "$0"`', file=file)
                print('source "$CURDIR/.venv/bin/activate"', file=file)
                relpath = os.path.relpath(self.PYTHON_EXEC, os.path.dirname(path))
                print(f'python "$CURDIR/{relpath}" "$@"', file=file)
        make_exec(path)

        os.chdir(wrapper_dir)

    def mod_postinst(self):
        super().mod_postinst()
        # Re-initialize virtualenv in the new location to update its paths
        os.chdir(os.path.join(self.ROOT, self.WRAPPER_DIR or "bin"))
        self.execute("virtualenv .venv")

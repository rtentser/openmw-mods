# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import sys
import shutil
import re
from pybuild import InstallDir, find_file


def bashconvert(file):

    with open(file, "r") as f:
        lines = f.readlines()

    def convertline(line):
        # Comment out unnecessary commands
        if re.search("^(::|REM|@echo|pause)", line):
            line = "# " + line

        line = line.strip()

        # Arithmetic
        line = re.sub(r"set /a (.*)", r"((\1))", line, flags=re.IGNORECASE)

        # Assigment
        line = re.sub(r"set (\w*)=", r"\1=", line, flags=re.IGNORECASE)

        # Remove other /. values.
        # For most arithmetic/boolean expressions bash inferences the type
        line = re.sub(" /. ", " ", line)
        line = re.sub(r"(?<!-format )\%\%", r"$", line)
        line = re.sub(r"-format \%\%", r"-format \%", line)
        line = re.sub(r"\%(\w+)\%", r"${\1}", line)
        # Needs -f as del ignores nonexistant files
        line = re.sub("^del ", "rm -f ", line, flags=re.IGNORECASE)
        line = re.sub("'(.*)'", r"`\1`", line)
        line = re.sub("^cd..$", "cd ..", line)

        line = re.sub(
            r"^if not exist (.*) mkdir (.*)",
            r"if [[ ! -r \1 ]]; then mkdir \2; fi",
            line,
            flags=re.IGNORECASE,
        )
        match = re.search(
            r"^for (?P<A>.*) in \((?P<B>.*)\) do (?P<C>.*)", line, flags=re.IGNORECASE
        )
        if match:
            var = match.group("A")
            var = var.lstrip("%$")
            iterable = match.group("B")
            iterable.replace("'", "`")
            command = match.group("C")
            if iterable.startswith("1,1,"):
                iterable = iterable.replace("1,1,", "")
                iterable = f"`seq {iterable}`"
            line = f"for {var} in {iterable}; do {command}; done"

        return line

    def convert_lines(lines):
        output = []
        while lines:
            line = lines[0]

            # Single line
            line = convertline(line)

            # Multi line
            match = re.match(r"if (.*) (\w*) (.*) \(", line, flags=re.IGNORECASE)
            if match:
                condition = match.group(1)
                ops = {"GEQ": ">=", "NEQ": "!=", "LEQ": "<=", "EQ": "=="}
                op = ops[match.group(2)]
                value = match.group(3)
                command = []
                del lines[0]
                line = lines[0]
                while not re.search(r"^\)$", line):
                    command.append(line)
                    del lines[0]
                    line = lines[0]
                line = ""
                output.append(f"if (( {condition} {op} {value} )); then")
                # Treat inner as its own thing
                output.extend(["    " + line for line in convert_lines(command)])
                output.append("fi")

            line = line.strip()

            output.append(line)
            del lines[0]
        return output

    return "\n".join(convert_lines(lines))


class AtlasGen:
    def __init__(self):
        self.IUSE = self.IUSE | {"atlasgen", "map_normal", "map_specular"}
        self.IUSE_EFFECTIVE = self.IUSE_EFFECTIVE | {
            "atlasgen",
            "map_normal",
            "map_specular",
        }
        self.INSTALL_DIRS.append(
            InstallDir("atlases", S="atlasgen", REQUIRED_USE="atlasgen")
        )

    def generate_atlases_oftype(self, typ, suffix=""):
        """
        Generates a set of atlases for the given suffix

        Suffixes are added to the fileset when searching,
        and included in the atlases at the end
        """
        os.makedirs(os.path.join(self.T, "textures"), exist_ok=True)
        os.chdir(os.path.join(self.T, "textures"))

        for install_dir, scriptfile in self.get_files("ATLASGEN"):
            scriptpath = os.path.join(
                self.WORKDIR, install_dir.S, install_dir.PATH, scriptfile.NAME
            )

            # Copy files that will be included in the atlas
            files = set()
            with open(scriptpath, "r") as file:
                for line in file.readlines():
                    for ddsfile in re.findall(
                        r'([\\/\w]+\.dds|"[\\/\w\s]+\.dds")', line, flags=re.IGNORECASE
                    ):
                        stripped = ddsfile.strip().strip('"')
                        if "atl" not in os.path.dirname(stripped).lower():
                            files.add(stripped)

            for file in files:
                file = file.strip()
                name, ext = os.path.splitext(file)
                src = find_file(os.path.join("textures", name + suffix + ext))
                if src:
                    shutil.copyfile(src, file)
                else:
                    raise FileNotFoundError(f"No file found to satisfy textures/{file}")

            if sys.platform == "win32":
                print(f"Running {os.path.basename(scriptpath)}...")
                self.execute(f'"{scriptpath}"')
            else:
                print(f"Converting {os.path.basename(scriptpath)} to bash...")
                script = bashconvert(scriptpath)

                with open("script.sh", "w") as file:
                    file.write(script)
                print("Running converted script...")
                self.execute("bash script.sh")
                os.remove("script.sh")

            # Remove the original texture files,
            # we only care about installing the atlases
            for file in files:
                try:
                    os.remove(file.strip())
                except FileNotFoundError:
                    continue

        # Install atlases
        dest = os.path.join(self.WORKDIR, "atlasgen", "atlases", "textures")
        os.makedirs(dest, exist_ok=True)
        for root, dirs, files in os.walk("."):
            for name in dirs:
                os.makedirs(os.path.join(dest, root, name), exist_ok=True)
            for file in files:
                name, ext = os.path.splitext(file)
                os.rename(
                    os.path.join(root, file),
                    os.path.join(dest, root, name + suffix + ext),
                )

    def src_prepare(self):
        super().src_prepare()
        if "atlasgen" in self.USE:
            self.generate_atlases_oftype("Textures")
            if "map_normal" in self.USE:
                self.generate_atlases_oftype("Normal Maps", "_n")
            if "map_specular" in self.USE:
                self.generate_atlases_oftype("Specular Maps", "_spec")

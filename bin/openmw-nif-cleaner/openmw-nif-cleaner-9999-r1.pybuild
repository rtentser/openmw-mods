# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pyclass import Git, Python
from pybuild.modinfo import MN


class Mod(Git, Python):
    NAME = "OpenMW NIF Cleaner"
    DESC = "An application to automatically fix normal maps to work with OpenMW"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "openmw tes3mp"
    HOMEPAGE = "https://github.com/treymerkley/openmw-nif-cleaner"
    GIT_SRC_URI = "https://github.com/treymerkley/openmw-nif-cleaner.git"
    S = MN
    PYTHON_EXEC = "fixed_fix_nif_console.py"
    PYTHON_RDEPEND = ["pyffi==2.2.2"]

    def src_prepare(self):
        super().src_prepare()
        # work around pyffi using `time.clock` which was removed in
        # python 3.8; this hack keeps it working though.  since the
        # pyffi code is frozen in time for the OpenMW NIF cleaner,
        # this is the least worst choice.
        with open("../fix_nif_console.py", "r") as original:
            with open("../fixed_fix_nif_console.py", "w") as fixed:
                print("import time", file=fixed)
                print("if getattr(time, 'clock', None) is None:", file=fixed)
                print("    time.clock = time.time", file=fixed)
                print("", file=fixed)
                print(original.read(), file=fixed)
